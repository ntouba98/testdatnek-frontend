export interface NotificationFinal {
    show: boolean;
    type: 'success' | 'danger' | 'warning';
    message: string;
};
export interface Notification {
    type: 'success' | 'danger' | 'warning';
    message: string;
};