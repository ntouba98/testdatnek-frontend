export interface HttpConfig {
    page: number;
    pagePossible: number[];
    precPagePossible: number | undefined;
    nextPagePossible: number | undefined;
    pages: string | number;
    pagesPossibles: (string | number)[];
    orderBy: string;
}