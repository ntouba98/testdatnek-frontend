export default interface LangInterface {
    lang: 'en' | 'fr' | 'nl';
    spoken_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    written_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    understanding_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
}