import { TestBed } from '@angular/core/testing';

import { LangApiService } from './lang-api.service';

describe('LangApiService', () => {
  let service: LangApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LangApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
