import { Injectable } from '@angular/core';
import Config from 'src/app/config/config.json';
import * as Axios from 'axios';
import { getHeaderRequest, responseEdit, responseSelect } from 'src/app/utils/http';
import { HttpConfig } from 'src/app/interfaces/http.interface';
import LangInterface from 'src/app/interfaces/lang.interface';

const axios = Axios.default;

@Injectable({
  providedIn: 'root'
})
export class LangApiService {
  findAllConfigDefault: HttpConfig = {
    page: 1,
    pagePossible: [],
    precPagePossible: 1,
    nextPagePossible: 1,
    pages: 'all',
    pagesPossibles: [],
    orderBy: 'lang_asc'
  };
  configHttp: HttpConfig = this.findAllConfigDefault;

  constructor(
  ) { }

  setConfigHttp(configHttp: HttpConfig) {
    this.configHttp = configHttp;
  }

  async findAll(configHttp: HttpConfig, lang: string): Promise<any> {
    this.setConfigHttp(configHttp);
    const stringUrl = 'test-datnek/langs';
    const myUrl = new URL(stringUrl, Config.server);
    myUrl.searchParams.append('lang', lang);
    myUrl.searchParams.append('page', String(configHttp.page));
    myUrl.searchParams.append('pages', 'all');
    myUrl.searchParams.append('status', 'visible');
    myUrl.searchParams.append('orderBy', String(configHttp.orderBy));
    return axios.get(
      String(myUrl),
      getHeaderRequest(),
    ).then((response: any) => responseSelect(response))
  }
  async find(id: string, lang: string) {
    const stringUrl = `test-datnek/langs/${id}`;
    console.log('----> find - stringUrl:: ', stringUrl);
    const myUrl = new URL(stringUrl, Config.server);
    myUrl.searchParams.append('lang', lang);
    return axios.get(
      String(myUrl),
      getHeaderRequest(),
    ).then((response: any) => responseSelect(response))
  }
  async delete(id: string, lang: string): Promise<any> {
    const stringUrl = `test-datnek/langs/archive/${id}`;
    const myUrl = new URL(stringUrl, Config.server);
    myUrl.searchParams.append('lang', lang);
    return axios.delete(
      String(myUrl),
      getHeaderRequest(),
    ).then((response: any) => responseEdit(response));
  }
  async edit(element: LangInterface, lang: string): Promise<any> {
    const stringUrl = `test-datnek/langs/edit`;
    const myUrl = new URL(stringUrl, Config.server);
    myUrl.searchParams.append('lang', lang);
    return axios.post(
      String(myUrl),
      element,
      getHeaderRequest(),
    ).then((response: any) => responseEdit(response));
  }
}
