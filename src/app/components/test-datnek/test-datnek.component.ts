import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
// import 'material.min.js';
import * as Axios from 'axios';
import { langs, levelsLang } from 'src/app/utils/config';
import DataLangConfig from '../../config/data-lang.json';
import LangInterface from 'src/app/interfaces/lang.interface';
import { UcFirst } from 'src/app/utils/string';
import { Notification, NotificationFinal } from 'src/app/interfaces/notification.interface';
import { HttpConfig } from 'src/app/interfaces/http.interface';
import { LangApiService } from 'src/app/services/lang-api/lang-api.service';

const axios = Axios.default;

@Component({
  selector: 'app-test-datnek',
  templateUrl: './test-datnek.component.html',
  styleUrls: ['./test-datnek.component.sass']
})
export class TestDatnekComponent extends LangApiService implements OnInit {
  dataLang: any = DataLangConfig;
  langNg: 'fr' | 'en' | 'nl' = 'fr';
  langs: string[] = langs;
  levelsLang: string[] = levelsLang;
  langNameSelected: string = 'fr';
  defaultElement: LangInterface = {
    lang: 'fr',
    spoken_level: 'current',
    written_level: 'current',
    understanding_level: 'current',
  };
  elementFind: LangInterface = this.defaultElement;
  elementSelected: LangInterface = this.defaultElement;
  elements: LangInterface[] = [];
  elementsLength: number = this.elements.length;
  elementsExists: boolean = this.elements.length > 0;
  modalDetailIsOpen: boolean = false;
  defaultNotification: NotificationFinal = {
    show: false,
    type: 'danger',
    message: 'message inconnu'
  };
  notification: NotificationFinal = this.defaultNotification;
  findAllConfig: HttpConfig = {
    page: 1,
    pagePossible: [],
    precPagePossible: 1,
    nextPagePossible: 1,
    pages: 'all',
    pagesPossibles: [],
    orderBy: 'lang_asc'
  };

  UcFirst = UcFirst;

  constructor(
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.initElements();
    this.getLangNg();
  }

  selectLang(lang: LangInterface) {
    this.elementSelected = lang;
    this.changeLangNg(this.elementSelected.lang);
  }
  getLangNg() {
    this.route.paramMap.subscribe(params => {
      console.log('----> getLangNg - params:: ', params);
      let lang: string | null = params.get("lang");
      console.log('----> getLangNg - lang:: ', params.get("lang"));
      lang = (langs.includes(String(lang))) ? lang : 'fr';
      this.langNg = lang as 'fr' | 'en' | 'nl';
      console.log('----> getLangNg - this.langNg:: ', this.langNg);
    });
  }
  changeLangNg(langName: string) {
    const elementSelected: LangInterface | undefined = this.elements.find((value: LangInterface) => {
      return value.lang === langName;
    });
    this.langNg = langName as 'fr' | 'en' | 'nl' || 'fr';
    this.elementSelected = (elementSelected) ? elementSelected : this.defaultElement;
  }
  initElements() {
    this.findAll(this.findAllConfigDefault, this.langNg).then((response: any) => {
      this.elements = response.datas;
      this.elementsLength = response.lenMax;
      this.elementsExists = response.exists;
      this.findAllConfig = {
        page: response.page,
        pagePossible: response.page,
        precPagePossible: response.page,
        nextPagePossible: response.page,
        pages: response.page,
        pagesPossibles: response.page,
        orderBy: response.page
      };
    }).catch(() => {
      this.findAllConfig = this.findAllConfigDefault;
      this.elements = [];
      this.elementsLength = 0;
      this.elementsExists = false;
    });
  }
  selectElement(id: string) {
    console.log('----> selectElement - id:: ', id);
    console.log('----> selectElement - this.langNg:: ', this.langNg);
    this.find(id, this.langNg).then((response: any) => {
      console.log('----> selectElement - response.data:: ', response.data);
      console.log('----> selectElement - response.exists:: ', response.exists);
      this.elementFind = (response.exists) ? {
        lang: response.data.lang,
        spoken_level: response.data.spoken_level,
        written_level: response.data.written_level,
        understanding_level: response.data.understanding_level,
      } : this.defaultElement;
      this.initElements();
    }).catch((err) => {
      console.log('----> selectElement - err:: ', err);
      this.elementFind = this.defaultElement;
      this.initElements();
    });
  }
  saveElement() {
    console.log('----> saveElement');
    this.edit(this.elementFind, this.langNg).then((response: any) => {
      const msg: Notification = response;
      console.log('----> saveElement - msg:: ', msg);
      this.setNotification(msg);
      this.initElements();
    }).catch(() => {
      this.setNotification({});
      this.initElements();
    });
  }
  deleteElement(element_code: string) {
    console.log('----> deleteElement');
    this.delete(element_code, this.langNg).then((response: any) => {
      const msg: Notification = response;
      console.log('----> deleteElement - msg:: ', msg);
      this.setNotification(msg);
      this.initElements();
    }).catch(() => {
      this.setNotification({});
      this.initElements();
    });
  }

  setNotification(msg: any) {
    const typeMsg = (['success', 'danger', 'warning'].includes(msg.type)) ? msg.type as ('success' | 'danger' | 'warning') : 'danger';
    this.notification = (
      typeof(msg) === 'object'
      && Array.isArray(msg) === false
      && Object.keys(msg).includes('type')
      && ['success', 'danger', 'warning'].includes(msg.type)
      && Object.keys(msg).includes('message')
      && typeof(msg.message) === 'string'
      && msg.message.length > 0
    ) ? {
      show: true,
      type: typeMsg,
      message: msg.message
    } : this.defaultNotification;
    console.log('----> setNotification - this.notification:: ', this.notification);
  }
  hideNotification() {
    this.notification = {
      show: false,
      type: 'danger',
      message: 'message inconnu'
    };
  }

}
