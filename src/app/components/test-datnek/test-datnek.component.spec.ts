import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDatnekComponent } from './test-datnek.component';

describe('TestDatnekComponent', () => {
  let component: TestDatnekComponent;
  let fixture: ComponentFixture<TestDatnekComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestDatnekComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDatnekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
