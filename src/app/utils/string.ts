export function UcFirst(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
}
export function Upper(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.toUpperCase();
}
export function Lower(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.toLowerCase();
}
export function Capitalize(value: any):string {
    const sep = ' ';
    if (!value) return '';
    const array_res:string[] = String(value).split(sep).map((val:string) => val.charAt(0).toUpperCase() + val.toLowerCase().slice(1));
    const res:string = array_res.join(sep);
    return res;
}