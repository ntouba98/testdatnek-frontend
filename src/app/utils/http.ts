import AppConfig from 'src/app/config/config.json';
// import { bus } from 'src/router/index';

export function getTokenAuth() {
    return window.sessionStorage.getItem(AppConfig.cache_name.auth);
}
export function setTokenAuth(token: string) {
    // console.log('> setTokenAuth - token :: ', token);
    // console.log('> setTokenAuth - AppConfig.cache_name.auth :: ', AppConfig.cache_name.auth);
    window.sessionStorage.setItem(AppConfig.cache_name.auth, token);
}
export function clearTokenAuth() {
    window.sessionStorage.removeItem(AppConfig.cache_name.auth);
}

export const tokenAuth = getTokenAuth();

export function getHeaderRequest(headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    accept: 'application/json',
}, others = {}): any {
    const result = {
        ...{
            headers: {
                ...headers,
                ...{
                    Authorization: getTokenAuth(),
                },
            },
        },
        ...others,
    };
//    const token = getTokenAuth();
//    // console.log('> responseToken - token :: ', token);
    // console.log('> getHeaderRequest - result:: ', result);
    return result;
}
export function getHeaderRequestDownload() {
    return getHeaderRequest({
        'Content-Type': 'application/json;charset=UTF-8',
        accept: 'application/json',
    }, {
        responseType: 'blob',
    });
}

export const errorEdit = {
    isHide: false,
    type: 'danger',
    message: 'impossibilité d\'accès au serveur',
};
export function cleanResponseSelect(dataResponse: any) {
    const result = (
        typeof dataResponse === 'object'
        && Array.isArray(dataResponse) === false
    ) ? dataResponse : {};
    result.columns = (
        Object.keys(result).includes('columns')
        && typeof result.columns === 'object'
        && Array.isArray(result.columns) === false
    ) ? result.columns : {
        initial: [],
    };
    result.data = (
        Object.keys(result).includes('data')
        && typeof result.data === 'object'
        && Array.isArray(result.data) === false
    ) ? result.data : {};
    result.datas = (
        Object.keys(result).includes('datas')
        && typeof result.datas === 'object'
        && Array.isArray(result.datas) === true
    ) ? result.datas : [];
    result.double = (
        Object.keys(result).includes('double')
        && typeof result.double === 'object'
        && Array.isArray(result.double) === false
    ) ? result.double : {
        detail: [],
        simplified: [],
    };
    result.invalid_datas = (
        Object.keys(result).includes('invalid_datas')
        && typeof result.invalid_datas === 'object'
        && Array.isArray(result.invalid_datas) === true
    ) ? result.invalid_datas : [];
    result.datas.forEach((row: any, index: number) => {
        row.index = index + 1;
    });
    result.lenMax = (
        typeof result.lenMax === 'number'
    ) ? result.lenMax : 1;
    result.len = (
        typeof result.len === 'number'
    ) ? result.len : 1;
    result.exists = (
        typeof result.exists === 'boolean'
    ) ? result.exists : false;
    result.page = (
        typeof result.page === 'number'
    ) ? result.page : 1;
    result.pagePossible = (
        typeof result.pagePossible === 'object'
        && Array.isArray(result.pagePossible) === true
    ) ? result.pagePossible : 1;
    result.precPagePossible = (
        typeof result.precPagePossible === 'number'
    ) ? result.precPagePossible : 1;
    result.nextPagePossible = (
        typeof result.nextPagePossible === 'number'
    ) ? result.nextPagePossible : 1;
    result.pages = (
        typeof result.pages === 'number'
        || result.pasges === 'all'
    ) ? result.pages : 25;
    result.pagesPossibles = (
        typeof result.pagesPossibles === 'object'
        && Array.isArray(result.pagesPossibles) === true
    ) ? result.pagesPossibles : 1;
    result.orderBy = (
        typeof result.orderBy === 'string'
        && result.orderBy.length > 0
    ) ? result.orderBy : 1;

    result.user = (
        Object.keys(result).includes('user')
        && typeof result.user === 'object'
        && Array.isArray(result.user) === false
    ) ? result.user : {};
    result.exists = (
        Object.keys(result).includes('exists')
        && typeof result.exists === 'boolean'
    ) ? result.exists : null;
    result.token = (
        Object.keys(result).includes('token')
        && typeof result.token === 'string'
        && result.token.length > 0
    ) ? result.token : undefined;

    result.type = (
        Object.keys(result).includes('type')
        && typeof result.type === 'string'
        && result.type.length > 0
    ) ? result.type : undefined;
    result.message = (
        Object.keys(result).includes('message')
        && typeof result.message === 'string'
        && result.message.length > 0
    ) ? result.message : undefined;
    /* if (result.exists === false) {
        result.type = 'auth-error';
    } */
    if (result.type === 'auth-error') {
        if (!window.sessionStorage.getItem(AppConfig.cache_name.auth)) {
            result.type = undefined;
            result.message = undefined;
        }
        window.sessionStorage.removeItem(AppConfig.cache_name.auth);
    }

    return result;
}
export function cleanResponseEdit(dataResponse: any) {
    let result = (
        typeof dataResponse === 'object'
        && Array.isArray(dataResponse) === false
    ) ? dataResponse : {};
    if (
        !(
            typeof result === 'object'
            && Array.isArray(result) === false
        )
    ) {
        result = errorEdit;
    }
    console.log('----> HTTP > cleanResponseEdit - result - old:: ', result);
    result.type = (
        Object.keys(result).includes('type')
        && typeof result.type === 'string'
        && result.type.length > 0
    ) ? result.type : errorEdit.type;
    result.message = (
        Object.keys(result).includes('message')
        && typeof result.message === 'string'
        && result.message.length > 0
    ) ? result.message : errorEdit.message;
    console.log('----> HTTP > cleanResponseEdit - result:: ', result);

    /* if (result.type === 'auth-error') {
        if (SessionStorage.has(AppConfig.cache_name.auth)) {
            result.type = undefined;
            result.message = undefined;
        }
        SessionStorage.remove(AppConfig.cache_name.auth);
    } */

    return result;
}

export function responseSelect(response: any) {
    const result = cleanResponseSelect(response.data);
    // console.log('******** http > responseSelect - result:: ', result);
    if (result.type && result.message) {
        result.isHide = false;
    } else {
        result.isHide = true;
    }

    return result;
}

export function responseDownload(response: any) {
    const responsesHeader = response.headers;
    const responseContentType = responsesHeader['content-type'];
    const responseFilename = responsesHeader['ac-filename'];
    // const contentDisposition = responsesHeader['Content-Disposition'];
    // console.log('> responseDownload - responseContentType:: ', responseContentType);
    // console.log('> responseDownload - responseFilename:: ', responseFilename);
    // console.log('> responseDownload - contentDisposition:: ', contentDisposition);
    // console.log('> responseDownload - responsesHeader:: ', responsesHeader);
    const blob = new Blob(
        [response.data],
        { type: responseContentType },
    );
    const fileURL = window.URL.createObjectURL(blob);
    const fileLink = document.createElement('a');

    fileLink.href = fileURL;
    fileLink.setAttribute('download', responseFilename);
    document.body.appendChild(fileLink);

    fileLink.click();
}

export function errSelect() {
//    // console.log(err);
    return [];
}

export function errEdit() {
//    // console.log('> errEdit');
//    // console.log(err);
    // bus.$emit('notification.action', errorEdit);
    return errorEdit;
}

export function responseEdit(response: any) {
    const result = cleanResponseEdit(response.data);
    return result;
}
export function responseLogout(response: any) {
    const result = cleanResponseEdit(response.data);
    if (result.type === 'success-logout') {
        clearTokenAuth();
    }

    return result;
}

export function responseToken(response: any) {
    const result = cleanResponseEdit(response.data);
    const tokenExist = (
        Object.keys(result).includes('token')
        && typeof result.token === 'string'
        && result.token.length > 0
    );
    // console.log('> responseToken - result :: ', result);
    // console.log('> responseToken - result.token :: ', result.token);
    // console.log('> responseToken - tokenExist :: ', tokenExist);

    if (tokenExist) {
        setTokenAuth(result.token);
        // const token = getTokenAuth();
        // console.log('> responseToken - token :: ', token);
        // console.log('> responseToken - TOKEN EXISTS ');
    }

    return result;
}